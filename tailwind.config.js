/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        navigation: "#E7EFF1",
        grayDark:"#555555",
        grayLight: "#8E8E8E",
        form: {
          back: "#9ED6E4"
        },
        btn: {
          primary: "#0283FA"
        },
        primary: {
          DEFAULT: "#0283FA",
        },
      },
    },
  },
  plugins: [],
}
