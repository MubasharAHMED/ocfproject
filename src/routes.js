const routes = {
  home: () => "/",
  courses: () => "/courses",
  signIn: () => "/signs/signIn",
  signUp: () => "/signs/signUp",
  forgotPassword: () => "/forgotPassword",
  accountDetail: () => "/account",
}

export default routes
