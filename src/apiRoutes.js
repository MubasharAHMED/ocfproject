const apiRoutes = {
  courses: {
    all: ()=>"/api/v1/courses",
  },
  login: ()=>"/api/v1/login"
}

export default apiRoutes
