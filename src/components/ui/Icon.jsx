import clsx from "clsx"

const colors = {
  black: "text-black",
  white: "text-white",
  primary: "text-primary",
  secondary: "text-secondary",
  danger: "text-danger",
  info: "text-info",
  success: "text-success",
  warning: "text-warning",
}

const sizes = {
  sm: "h-6 w-6",
  md: "h-8 w-8",
  lg: "h-10 w-10",
  xl: "h-14 w-14",
}

const Icon = (props) => {
  const {
    icon: Icon,
    color = "black",
    size = "md",
    className,
    ...svgProps
  } = props

  return (
    <Icon
      {...svgProps}
      className={clsx(colors[color], sizes[size], className)}
    />
  )
}

export default Icon
