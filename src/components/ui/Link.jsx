import NextLink from "next/link"

const Link = (props) => {
  const { className, href, disabled, ...otherProps } = props

  const realHref = typeof href === "function" ? href() : href

  if (disabled) {
    return <span className={`${className} disabled`} {...otherProps} />
  }

  return <NextLink {...otherProps} href={realHref} className={className} />
}

export default Link
