import clsx from "clsx"

const colors = {
  primary: "text-primary",
  secondary: "text-secondary",
  danger: "text-danger",
  warning: "text-warning",
  success: "text-success",
  info: "text-info",
  white: "text-white",
  slate: "text-slate-500",
}
const sizes = {
  xs: "text-xs",
  sm: "text-sm",
  md: "text-base",
  lg: "text-2xl",
  xl: "text-4xl",
}
const Text = (props) => {
  const { size = "md", color = "black", className, ...otherProps } = props

  return (
    <p
      className={clsx(colors[color], sizes[size], className)}
      {...otherProps}
    />
  )
}

export default Text
