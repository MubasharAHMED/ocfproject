import clsx from "clsx"

const colors = {
  primary:
    "rounded bg-primary text-white active:bg-primary-active hover:bg-primary-hover disabled:bg-primary-disabled focus:outline-none focus:ring focus:ring-primary-focus",
  secondary:
    "rounded bg-secondary text-white active:bg-secondary-active hover:bg-secondary-hover disabled:bg-secondary-disabled focus:outline-none focus:ring focus:ring-secondary-focus",
  danger:
    "rounded bg-danger text-white active:bg-danger-active hover:bg-danger-hover disabled:bg-danger-disabled focus:outline-none focus:ring focus:ring-danger-focus",
  warning:
    "rounded bg-warning text-white active:bg-warning-active hover:bg-warning-hover disabled:bg-warning-disabled focus:outline-none focus:ring focus:ring-warning-focus",
  success:
    "rounded bg-success text-white active:bg-success-active hover:bg-success-hover disabled:bg-success-disabled focus:outline-none focus:ring focus:ring-success-focus",
  info: "rounded bg-info text-white active:bg-info-active hover:bg-info-hover disabled:bg-info-disabled focus:outline-none focus:ring focus:ring-info-focus",
}

const sizes = {
  xs: "text-sm font-medium px-2 py-2",
  sm: "text-sm font-medium px-4 py-2",
  md: "text-base font-medium px-6 py-2",
}

const Button = (props) => {
  const { color = "primary", size = "md", className, ...otherProps } = props

  return (
    <button
      className={clsx(colors[color], className, sizes[size])}
      {...otherProps}
    />
  )
}

export default Button
