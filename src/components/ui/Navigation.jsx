import Link from "@/components/ui/Link.jsx"
import Text from "@/components/ui/Text.jsx"
import routes from "@/routes.js"
import Image from "next/image.js"

const mainRoutes = [
  {
    src: "/logos/icons8_home.svg",
    alt: "logo_classes",
    title: "Accueil",
    route: routes.home(),
  },
  {
    src: "/logos/icons8_writing.svg",
    alt: "logo_classes",
    title: "Mes Cours",
    route: routes.courses(),
  },
  {
    src: "/logos/icons8_textbook.svg",
    alt: "logo_classes",
    title: "Mes Chapitres",
    route: routes.home(),
  },
  {
    src: "/logos/icons8_classes.svg",
    alt: "logo_classes",
    title: "Mes Classes",
    route: routes.home(),
  },
  {
    src: "/logos/icons8_setting.svg",
    alt: "logo_classes",
    title: "Paramètres",
    route: routes.home(),
  },
]

const Navigation = () => {
  return (
    <div className="bg-navigation w-1/4 h-screen flex flex-col">
      <div className="h-1/3 flex justify-center items-center">
        <Link href={routes.home()} className="flex flex-row space-x-2">
          <Image
            src="/logos/icons8_main.svg"
            alt="logo_classes"
            width={24}
            height={24}
            priority
          />
          <Text size="lg" className="font-bold text-textGray">
            GenialNotes
          </Text>
        </Link>
      </div>

      <div className="flex flex-col items-center justify-center space-y-5">
        {mainRoutes.map((logo, index) => (
          <Link
            key={index}
            href={logo.route}
            className="flex flex-row items-center space-x-3"
          >
            <Image
              src={logo.src}
              alt={logo.alt}
              width={24}
              height={24}
              priority
            />
            <Text size="lg" className="text-textGray">
              {logo.title}
            </Text>
          </Link>
        ))}
      </div>
    </div>
  )
}

export default Navigation
