import clsx from "clsx"
import { useField } from "formik"
import Input from "./Input"

const FormField = (props) => {
  const {
    size,
    className,
    placeholder = "",
    label,
    prefixIcon,
    sufixIcon,
    name,
    rows,
    ...otherProps
  } = props
  const [field, { touched, error }] = useField({ name })

  return (
    <label className={clsx("relative flex flex-col gap-2", className)}>
      {label ? <span className="font-semibold">{label}</span> : null}
      <Input
        size={size}
        placeholder={placeholder}
        prefixIcon={prefixIcon}
        sufixIcon={sufixIcon}
        name={name}
        rows={rows ?? 1} // Set default to 1 row if not provided
        {...field}
        {...otherProps}
      />
      {touched && error ? (
        <span className="text-sm text-red-600">{error}</span>
      ) : null}
    </label>
  )
}

export default FormField
