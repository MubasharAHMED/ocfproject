import clsx from "clsx"
import Icon from "./Icon"
const Input = (props) => {
  const {
    size,
    className,
    placeholder = "",
    label,
    prefixIcon,
    sufixIcon,
    errorMessage,
    width,
    ...otherProps
  } = props

  const sizes = {
    xs: "h-2",
    sm: "h-8",
    md: "h-10",
  }

  const widths = {
    xs: "w-2",
    sm: "w-8",
    md: "w-9",
  }

  return (
    <>
      <label className={clsx("relative p-2 flex flex-col gap-2", className)}>
        {label ? <span className="font-semibold">{label}</span> : null}
      </label>
      <input
        className={clsx(
          "focus:outline-none focus:ring-1 focus:ring-sky-700 focus:border-transparent hover:bg-slate-100 disabled:bg-slate-200 w-full ",
          sizes[size],
          widths[width],
          errorMessage ? "border-danger" : null,
          prefixIcon ? "pl-8" : null
        )}
        {...otherProps}
        placeholder={placeholder}
      />
      {prefixIcon ? (
        <Icon
          icon={prefixIcon}
          size="sm"
          className="absolute inset-y-12 left-3"
        />
      ) : null}
      {sufixIcon ? (
        <Icon
          icon={sufixIcon}
          size="sm"
          className="absolute inset-y-12 right-3"
        />
      ) : null}

      {errorMessage ? <p className="text-red-900">{errorMessage}</p> : null}
    </>
  )
}

export default Input
