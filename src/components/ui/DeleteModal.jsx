import { Dialog } from "@headlessui/react"
import Button from "@@/ui/Button"
import Text from "@@/ui/Text"

const DeleteModal = (props) => {
  const { open, onRequestClose, handleDeleteData } = props
  const handleCloseModal = () => {
    onRequestClose()
  }

  return (
    <Dialog
      open={open}
      onClose={handleCloseModal}
      className="fixed inset-0 z-10 overflow-y-auto flex justify-center items-center min-h-screen"
    >
      <Dialog.Overlay className="fixed inset-0 bg-black opacity-30" />
      <Dialog.Panel className="relative bg-white rounded-lg shadow-md p-8 w-full md:w-1/3 mt-16">
        <Dialog.Title className="flex justify-center font-bold text-lg mb-4">
          Supprimer un cours
        </Dialog.Title>

        <Text as="h2" className="my-2">
          Êtes-vous certain de vouloir supprimer ce cours? Les cours supprmiés
          seront gardés dans la corbeille pendant 30 jours.
        </Text>
        <div className="flex justify-around">
          <Button
            size="md"
            className="p-2 bg-white border border-grayDark"
            onClick={handleCloseModal}
          >
            <Text className="text-grayDark">Garder</Text>
          </Button>
          <Button size="md" className="p-2" onClick={handleDeleteData}>
            Supprimer
          </Button>
        </div>
      </Dialog.Panel>
    </Dialog>
  )
}

export default DeleteModal
