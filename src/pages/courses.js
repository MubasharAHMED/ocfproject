require("dotenv").config()
import Button from "@@/ui/Button.jsx"
import Navigation from "@@/ui/Navigation.jsx"
import Text from "@@/ui/Text.jsx"
import Input from "@/components/ui/Input.jsx"
import Image from "next/image.js"
import { useEffect, useState } from "react"
import axios from "axios"
import apiRoutes from "@/apiRoutes.js"
import { useRouter } from "next/router"
import routes from "@/routes.js"
import Link from "@/components/ui/Link.jsx"
import { Dialog } from "@headlessui/react"
import DeleteModal from "@/components/ui/DeleteModal.jsx"

const coursesExample = [
  { Name: "GOLANG"},
  { Name: "Python" },
  { Name: "React" },
  { Name: "NextJS" },
  { Name: "Rust" },
  { Name: "PHP"},
]

export default function Home() {
  const router = useRouter()
  const [courses, setCourses] = useState(null)
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false)
  

  useEffect(() => {
    const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI3NzRiYzM0Ny04MWNjLTRlOTYtODMyMS1iODk3MDQ5ZjNiOWUiLCJleHAiOjE2ODg3MTc4NDB9.Nhqa_Mherni5Jn8PGk8OPTPKnIb6L-DPqcBFbJivJwk"
      
      axios
        .get("http://localhost:8000/api/v1/courses", {
          headers: {
            "Accept": "application/json",
            "Authorization": `Bearer ${token}`,
          },
        })
        .then((response) => {
          const { result } = response.data
          setCourses(result)
        })
        .catch((error) => {
          console.log("Erreur lors de la récupération des cours de l'api, utilisation de l'exemple :", error)
          setCourses(coursesExample)
        })
  },[])

  const handleDelete = () => {
    //deleting element
  }

  const handleDeleteModal = () => {
    setIsDeleteModalOpen(true)
  }

  const handleCloseFilterModal = () => {
    setIsDeleteModalOpen(false)
  }

  return (
    <>
      <div className="flex flex-row overflow-hidden">
        <Navigation />
        <div className="p-4 mt-4 flex flex-col w-full ">

        <div className="flex flex-row justify-between">
              <Text size="lg" className="font-semibold">Bienvenue Emir,</Text>
              <div>
                <Text size="lg" className="font-semibold">Emir DELJANIN</Text>
                <Text className="font-bold" color="primary">Professeur</Text>
              </div>
            </div>
        
        <div className="mt-10 flex flex-row justify-between">
          <Text className="font-semibold" size="lg">Liste des cours</Text>
          <div>
            <Button size="sm" className="p-2">Ajouter un cours</Button>
          </div>
          </div>
          
          <div className="border rounded px-2 flex flex-row mt-2">
            <Image
              src="/logos/icons8_search.svg"
              alt="logo_classes"
              width={24}
              height={24}
              priority
            />

            <Input size="md" placeholder="Rechercher un cours ..." className=""/>
          </div>

          
          {courses ? (
            <div className="mt-12 h-3/6 overflow-y-auto">
            <div className=" space-y-9 font-sans ">
                {courses.map((course, index) => (
                  <div key={index} className="shadow-md p-4 flex justify-between border rounded h-24">
                    <div>
                      <Text size="lg" className="">{course.Name}</Text>
                      <Text size="sm">Modifié il y a {Math.floor(Math.random() * 5) + 1} jours</Text>
                    </div>
                    <div className="flex flex-row space-x-3">
                      <Link className="flex flex-row items-center space-x-3" href="">
                        <Image
                          src="/logos/icons8_edit.svg"
                          alt="modifier"
                          width={24}
                          height={24}
                          priority
                        />
                      </Link>
                      
                      <Link className="flex flex-row items-center space-x-3" href="">
                        <Image
                          src="/logos/icons8_download.svg"
                          alt="télécharger"
                          width={24}
                          height={24}
                          priority
                        />
                      </Link>
                  
                      <Link className="flex flex-row items-center space-x-3" href="" onClick={()=>handleDeleteModal()}>
                        <Image
                          src="/logos/icons8_delete.svg"
                          alt="supprimer"
                          width={24}
                          height={24}
                          priority
                        />
                      </Link>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          ) : (
              <div className="flex flex-col items-center space-y-4">
                <Image
                  src="/logos/icons8_new-copy.svg"
                  alt="logo_classes"
                  width={200}
                  height={40}
                  priority
                />
                <Text className="text-textGray">Vous n’avez pas encore de cours</Text>
                <Button size="sm" className="p-2">Ajouter un cours</Button>
              </div>
          )}
          
          <DeleteModal
            open={isDeleteModalOpen}
            onRequestClose={handleCloseFilterModal}
            handleDeleteData={handleDelete}
          />
        </div>
      </div>
    </>
  )
}