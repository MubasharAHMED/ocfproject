require("dotenv").config()
import { useState } from "react"
import { Form, Formik } from "formik"
import Link from "@/components/ui/Link.jsx"
import Button from "@@/ui/Button.jsx"
import FormField from "@/components/ui/FormField"
import Text from "@/components/ui/Text.jsx"
import axios from "axios"
import routes from "@/routes"
import { useRouter } from "next/router"
import apiRoutes from "@/apiRoutes.js"
import Image from "next/image.js"

export default function SignIn() {
  const router = useRouter()

  const handleSubmitSignIn = async (values, { resetForm }) => {
        const response = await axios.post(process.env.API_URL+ apiRoutes.login(), {
          email: values.email,
          password: values.password,
        })

        const { token, refresh_token } = response.data

        localStorage.setItem("token", token)
        localStorage.setItem("refresh_token", refresh_token)

        router.push(routes.home())
    resetForm()
  }

  return (
    <>
      <div className="flex justify-center">
        
        <div className="flex flex-col items-center justify-center min-h-screen w-1/3 ">
         
          <div className="w-full p-2">

           <Link href={routes.home()} className="flex flex-row space-x-2">
          <Image
            src="/logos/icons8_main.svg"
            alt="logo_classes"
            width={24}
            height={24}
            priority
          />
          <Text size="lg" className="font-bold text-textGray">
            GenialNotes
          </Text>
        </Link>
          </div>
          <div className="bg-form-back/50 shadow-lg rounded-lg p-8">
            <Text size="lg" className="flex justify-center text-grayDark">
              Connexion
            </Text>
          
            <Formik
              
              initialValues={{
                email: "",
                password: "",
              }}
              onSubmit={(values, { resetForm }) =>
                handleSubmitSignIn(values, { resetForm })
              }
            >
              {() => (
                <Form>
                  <FormField
                    size="md"
                    type="email"
                    name="email"
                    placeholder={"email"}
                    className="w-full"
                    required
                  />
                  <FormField
                    size="md"
                    type="password"
                    name="password"
                    placeholder={"password"}
                    className="w-full"
                    required
                  />
                  <div className="flex justify-center">
                    <Button
                      type="submit"
                      className="mt-6 rounded-3xl  bg-btn-primary/50 w-full"
                      size="sm"
                      >
                      <Text className="text-grayDark">Se connecter</Text>
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </>
  )
}
