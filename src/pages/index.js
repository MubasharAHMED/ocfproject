import Navigation from "@/components/ui/Navigation.jsx"
import Text from "@/components/ui/Text.jsx"

export default function Home() {
    return (
      <>
        <div className="flex flex-row">
          <Navigation />
          <div className="p-4 mt-4 flex flex-col w-full ">

             <div className="flex flex-row justify-between">
              <Text size="lg" className="font-semibold">Bienvenue Emir,</Text>
              <div>
                <Text size="lg" className="font-semibold">Emir DELJANIN</Text>
                <Text className="font-bold" color="primary">Professeur</Text>
              </div>
            </div>
          </div>
        </div>
      </>
    )
}
