const coursesData = [
  {
    Name: "GOLANG",
    Theme: "sdv",
    Format: 0,
    AuthorEmail: "",
    Category: "",
    Version: "v3.0",
    Title: "Golang, premiers pas",
    Subtitle: "Un langage simple et efficace",
    Header: "Golang, premiers pas",
    Footer: "2023 - Golang @@version@@ - @@author_fullname@@ - @@author_email@@",
    Logo: "",
    Description: "imported from https://gitlab.com/open-course-factory/ocf-course-golang-beginner",
    CourseID_str: "",
    Schedule: "sdv_m.html",
    Prelude: "sdv.md",
    learning_objectives: "",
    chapters: [
      {
        id: "4b469a25-7a79-41fb-a2e6-5529cb964789",
        title: "Introduction",
        number: 0,
        footer: "@@author_fullname@@ - Golang - Introduction",
        introduction: "Dans lequel nous aborderons les concepts de base de Golang",
        sections: [
          {
            id: "c64dc9c8-aabb-45a8-bd9f-f58366f57c25",
            fileName: "prog/golang_introduction.md",
          },
          // Autres sections...
        ],
      },
      // Autres chapitres...
    ],
  },
  {
    Name: "PYTHON",
    Theme: "sdv",
    Format: 0,
    AuthorEmail: "",
    Category: "",
    Version: "v2.0",
    Title: "Python, les fondamentaux",
    Subtitle: "Un langage puissant et polyvalent",
    Header: "Python, les fondamentaux",
    Footer: "2023 - Python @@version@@ - @@author_fullname@@ - @@author_email@@",
    Logo: "",
    Description: "imported from https://gitlab.com/open-course-factory/ocf-course-python-fundamentals",
    CourseID_str: "",
    Schedule: "sdv_m.html",
    Prelude: "sdv.md",
    learning_objectives: "",
    chapters: [
      {
        id: "8a2e746d-2f82-4ae6-89d3-3e25e0dfcc7a",
        title: "Introduction",
        number: 0,
        footer: "@@author_fullname@@ - Python - Introduction",
        introduction: "Dans lequel nous aborderons les bases de Python",
        sections: [
          {
            id: "e9e9b225-438f-41c2-87ab-0917911d3698",
            fileName: "prog/python_basics.md",
          },
          // Autres sections...
        ],
      },
      // Autres chapitres...
    ],
  },
  {
    Name: "JAVASCRIPT",
    Theme: "sdv",
    Format: 0,
    AuthorEmail: "",
    Category: "",
    Version: "v1.0",
    Title: "JavaScript, les fondamentaux",
    Subtitle: "Le langage de programmation web",
    Header: "JavaScript, les fondamentaux",
    Footer: "2023 - JavaScript @@version@@ - @@author_fullname@@ - @@author_email@@",
    Logo: "",
    Description: "imported from https://gitlab.com/open-course-factory/ocf-course-javascript-fundamentals",
    CourseID_str: "",
    Schedule: "sdv_m.html",
    Prelude: "sdv.md",
    learning_objectives: "",
    chapters: [
      {
        id: "54272548-0dbd-4d43-b663-5b8c68276f72",
        title: "Introduction",
        number: 0,
        footer: "@@author_fullname@@ - JavaScript - Introduction",
        introduction: "Dans lequel nous aborderons les bases de JavaScript",
        sections: [
          {
            id: "e441c98d-5ea1-4e59-9302-b80e7438f239",
            fileName: "prog/javascript_basics.md",
          },
          // Autres sections...
        ],
      },
      // Autres chapitres...
    ],
  },
]

export default coursesData
